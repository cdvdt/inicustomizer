using System;
using System.Text.RegularExpressions;

namespace IniCustomizer.src.model
{
    class Section : IAddable
    {
        private bool isCommented;
        private string name;

        public Section(string name, bool isCommented = false)
        {
            this.isCommented = isCommented;
            this.name = name;
        }

        /// <summary>
        /// Parses a string to a valid <cref>Section</cref>
        /// </summary>
        /// <param name="value">value to be parsed</param>
        public Section(string value)
        {
            if (IsSection(value))
            {
                name = Regex.Match(value, @"\w+").Value;
                isCommented = Regex.IsMatch(value, @"\s*#.*");
            }
            else
                throw new InputDataNotValidException();
        }

        public string Content => (IsCommented ? "#" : "") + "[" + name + "]";

        public bool IsCommented { get => isCommented; set => isCommented = value; }
        public string Name { get => name; set => name = value; }

        public bool IsCommentable => true;

        public static bool IsSection(string value) => Regex.IsMatch(value, @"^\s*#?\s*\[\w+\]\s*$");
    }
}
