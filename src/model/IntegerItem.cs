﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IniCustomizer.src.model
{
    class IntegerItem : Item
    {
        private int value;

        public IntegerItem(string key, int value, bool isCommented = false) : base(key, isCommented)
        {
            this.value = value;
        }

        /// <summary>
        /// Parses a string to a valid <cref>IntegerItem</cref>
        /// </summary>
        /// <param name="value">value to be parsed</param>
        public IntegerItem(string value) : base("", false)
        {
            if (IsIntegerItem(value))
            {
                Key = Regex.Match(value, @"\w+").Value;
                this.value = int.Parse(Regex.Match(value, @"\d+").Value);
                IsCommented = Regex.IsMatch(value, @"\s*#.*");
            }
            else
                throw new NotImplementedException();
        }

        public int Value { get => value; set => this.value = value; }

        public override string Content => (IsCommented ? "#" : "") + Key + "=" + value;

        public static bool IsIntegerItem(string value) => Regex.IsMatch(value, @"^\s*#?\s*\w+=\d+\s*$");
    }
}
