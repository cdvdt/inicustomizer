﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IniCustomizer.src.model
{
    class Whitespace : IAddable
    {
        public bool IsCommented { get => false; set { } }

        public string Content => "";

        public bool IsCommentable => false;

        public static bool IsWhitespace(string value) => Regex.IsMatch(value, @"^\s*$");
    }
}
