﻿using System.Text.RegularExpressions;

namespace IniCustomizer.src.model
{
    class Comment : IAddable
    {
        private string content;

        public Comment(string content)
        {
            this.content = content;
        }

        public static Comment FromString(string value)
        {
            if (IsComment(value))
                return new Comment(Regex.Match(value, @"#(.*)").Groups[1].Value);
            else
                throw new InputDataNotValidException();
        }

        public bool IsCommented { get => true; set { } }

        public string Content { get => "#" + content; set => content = value; }

        public bool IsCommentable => false;

        public static bool IsComment(string value) => Regex.IsMatch(value, @"^\s*#.*$");
    }
}
