﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IniCustomizer.src.model
{
    class StringItem : Item
    {
        private string value;

        public StringItem(string key, string value, bool isCommented = false) : base(key, isCommented)
        {
            this.value = value;
        }

        /// <summary>
        /// Parses a string to a valid <cref>StringItem</cref>
        /// </summary>
        /// <param name="value">content to be parsed</param>
        public StringItem(string value) : base("", false)
        {
            if (IsStringItem(value))
            {
                Key = Regex.Match(value, @"\w+").Value;
                this.value = Regex.Match(value, @"\w+=(.*)").Groups[1].Value;
                IsCommented = Regex.IsMatch(value, @"\s*#.*");
            }
            else
                throw new NotImplementedException();
        }

        public string Value { get => value; set => this.value = value; }

        public override string Content => (IsCommented ? "#" : "") +  Key + "=" + value + "";

        public static bool IsStringItem(string value) => Regex.IsMatch(value, @"^\s*#?\s*\w+=.*\s*$");
    }
}
