﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniCustomizer.src.model
{
    class AddableFactory
    {
        /// <summary>
        /// Parses a string and returns the valid <cref>Addable</cref> value for case, if any
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <returns></returns>
        public static IAddable FromString(string value, bool ForceUnrecognisedAsComment = false)
        {
            if (Section.IsSection(value))
                return new Section(value);
            else if (ItemFactory.IsItem(value))
                return ItemFactory.FromString(value);
            else if (Comment.IsComment(value))
                return Comment.FromString(value);
            else if (Whitespace.IsWhitespace(value))
                return new Whitespace();
            else if (ForceUnrecognisedAsComment)
                return new Comment(value);
            else
                throw new NotImplementedException();
        }
    }
}
