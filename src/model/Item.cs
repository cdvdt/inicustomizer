﻿using System;

namespace IniCustomizer.src.model
{
    abstract class Item : IAddable
    {
        private Boolean isCommented;
        private string key;

        protected Item(string key, bool isCommented = false)
        {
            this.key = key;
            this.isCommented = isCommented;
        }

        public string Key { get => key; set => key = value; }
        public bool IsCommented { get => isCommented; set => isCommented = value; }

        public abstract string Content { get; }

        public bool IsCommentable => true;
    }
}
