﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniCustomizer.src.model
{
    class ItemFactory
    {
        private ItemFactory() { }

        public static bool IsItem(string value)
        {
            if (IntegerItem.IsIntegerItem(value)) 
                return true;
            else if (StringItem.IsStringItem(value))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Parses a string and returns the valid <cref>Item</cref> value for case, if any
        /// </summary>
        /// <param name="value">value to be parsed</param>
        /// <returns></returns>
        public static Item FromString(string value, bool ForceUnrecognisedAsComment = false)
        {
            if (IntegerItem.IsIntegerItem(value))
                return new IntegerItem(value);
            else if (StringItem.IsStringItem(value))
                return new StringItem(value);
            else
                throw new NotImplementedException();
        }
    }
}
