﻿using System;

namespace IniCustomizer.src.model
{
    public class InputDataNotValidException : Exception { }

    interface IAddable
    {
        bool IsCommented
        {
            get;
            set;
        }

        string Content
        {
            get;
        }

        bool IsCommentable { get; }
    }
}
