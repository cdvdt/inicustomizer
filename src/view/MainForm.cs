using IniCustomizer.src.controller;
using IniCustomizer.src.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IniCustomizer
{
    public partial class MainForm : Form
    {
        private Content content;
        private string fileName = "";
        private bool modified = false;

        public MainForm(CommandlineConfiguration commandlineConfiguration)
        {
            InitializeComponent();
            parseWindowText();
            var filename = commandlineConfiguration.Filename;
            if (filename == "") newFile(); else openFile(filename);
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newFile();
        }

        private void newFile()
        {
            content = new Content();
            // section added as example of the classes implemented, to be removed from final application
            content.Items.Add(AddableFactory.FromString("[GERAL]"));
            content.Items.Add(AddableFactory.FromString("#desevolvimento"));
            content.Items.Add(AddableFactory.FromString("#PATH=C:\\SomePlace\\Database\\database_old.FDB"));
            content.Items.Add(AddableFactory.FromString("PATH=C:\\SomePlace\\Database\\database.FDB"));
            content.Items.Add(AddableFactory.FromString(""));
            content.Items.Add(AddableFactory.FromString("[SERVIDOR]"));
            content.Items.Add(AddableFactory.FromString("PORT=3050"));
            // end of section 
            dgvContent.DataSource = content.Items;
            notifySavedLoaded();
        }

        private void dgvContent_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            IAddable item = dgvContent.CurrentRow.DataBoundItem as IAddable;
            if (item.IsCommentable)
            {
                notifyModified();
                dgvContent.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            else
                dgvContent.CancelEdit();
            dgvContent.Refresh();
        }

        private void dgvContent_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgvContent.DataSource != null)
                if ((dgvContent.Rows[e.RowIndex].DataBoundItem as IAddable).IsCommented)
                    e.CellStyle.ForeColor = Color.Green;
        }

        private void openFile(string fileName)
        {
            this.fileName = fileName;
            content = ContentController.ContentFromFile(fileName);
            dgvContent.DataSource = content.Items;
            notifySavedLoaded();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // save your current directory  
            string currentDirectory = Environment.CurrentDirectory;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = Environment.CurrentDirectory;
                openFileDialog.Filter = "ini files (*.ini)|*.ini|cfg files (*.cfg)|*.cfg|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = false;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                    openFile(openFileDialog.FileName);
            }
        }

        private void saveAsContent()
        {
            using (var saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Filter = "ini files (*.ini)|*.ini|cfg files (*.cfg)|*.cfg|All files (*.*)|*.*";
                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = false;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    ContentController.ContentToFile(content, saveFileDialog.FileName);
                    notifySavedLoaded();
                }
            }
        }

        private void salvarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fileName == "")
                saveAsContent();
            else
            {
                ContentController.ContentToFile(content, fileName);
                notifySavedLoaded();
            }

        }

        private void salvarcomoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveAsContent();
        }

        private void parseWindowText()
        {
            Text = "IniCustomizer (" + (fileName == "" ? "Untitled" : fileName) + ")" + (modified ? "*" : "");
        }

        private void notifyModified()
        {
            modified = true;
            parseWindowText();
        }

        private void notifySavedLoaded()
        {
            modified = false;
            parseWindowText();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var size = btnAdd.Size;
            btnAdd.ContextMenuStrip.Show(btnAdd, size.Width / 2, size.Height / 2);
        }
    }
}
