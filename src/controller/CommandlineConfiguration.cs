﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniCustomizer.src.controller
{
    public class CommandlineConfiguration
    {
        private string filename = "";
        public CommandlineConfiguration(string[] args)
        {
            foreach (var arg in args)
                if (!arg.StartsWith("-") && (filename == ""))
                    filename = arg;
        }

        public string Filename { get => filename; set => filename = value; }
    }
}
