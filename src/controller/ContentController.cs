﻿using IniCustomizer.src.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniCustomizer.src.controller
{
    class ContentController
    {
        private ContentController() { }

        public static Content ContentFromFile(string path)
        {
            var content = new Content();
            using (var reader = new System.IO.StreamReader(path))
            {
                while (!reader.EndOfStream)
                    content.Items.Add(AddableFactory.FromString(reader.ReadLine()));
            }

            return content;
        }

        public static void ContentToFile(Content content, string path)
        {
            var contentText = new StringBuilder();

            foreach (var item in content.Items)
                contentText.AppendLine(item.Content);

            File.WriteAllText(path, contentText.ToString());
        }
    }
}
